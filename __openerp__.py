{
    "name": "Tea Farm Labor Management",
    "summary": "Workers, Tea plucking records, Payments, Reports",
    "description": "Keeps tea plucking records, payments in a tea farm",
    "author": "Oscar Koech",
    "website": "https://github.com/koechoscar/tea",
    "application": True,
    "installable": True,
    "category": "Human Resources",
    "data": [
        'views/tea.xml',
        'views/labor.xml',
        'views/payment.xml',
        'views/worker.xml',
        'views/settings.xml',
    ],
    "depends": ['hr']
}
