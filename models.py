from openerp import models, fields, api, exceptions


class Worker(models.Model):
    _inherit = 'hr.employee'
    is_tea_worker = fields.Boolean(string="Tea Worker?", default=False)

    outstanding_wage = fields.Float(string='Outstanding Tea Farm Wage', compute='compute_outstanding_wage')

    # Labor and Payment Records
    labor_ids = fields.One2many('tea.labor', 'worker_id', string='Labor Records')
    payment_ids = fields.One2many('tea.payment', 'worker_id', string='Payment Records')

    @api.one
    def compute_outstanding_wage(self):
        total_wage = 0
        total_paid = 0
        # Sum wage for all work delivered
        for labor in self.labor_ids:
            total_wage += labor.wage_amount
        # Sum all payments made to worker
        for payment in self.payment_ids:
            total_paid += payment.amount
        # Set outstanding wage to (total_wage-total_paid)
        self.outstanding_wage = total_wage - total_paid


class Labor(models.Model):
    _name = "tea.labor"
    _rec_name = "id"
    worker_id = fields.Many2one('hr.employee', string="Worker", domain=[('is_tea_worker', '=', True)], required=True,
                                ondelete='restrict')
    tea_weight = fields.Float(string="Tea Weight", required=True)
    unit_pay = fields.Float(string='Unit Pay', required=True, readonly=1)
    wage_amount = fields.Float(string='Wage Amount', compute='compute_wage_amount', store=False)

    @api.one
    @api.constrains('tea_weight')
    def validate_tea_weight(self):
        if self.tea_weight <= 0:
            raise exceptions.ValidationError('Tea weight must be greater than zero')

    @api.one
    @api.constrains('unit_pay')
    def validate_unit_pay(self):
        if self.unit_pay <= 0:
            raise exceptions.ValidationError(
                'Unit pay must be greater than zero. '
                'Please go to Tea Farm > Configuration > Settings and set the Unit Pay'
            )

    @api.one
    @api.depends('tea_weight', 'unit_pay')
    def compute_wage_amount(self):
        self.wage_amount = self.tea_weight * self.unit_pay


class Payment(models.Model):
    _name = "tea.payment"
    _rec_name = "id"
    worker_id = fields.Many2one('hr.employee', string="Worker", required=True, ondelete='restrict')
    amount = fields.Float(string="Amount Paid", required=True)

    @api.one
    @api.constrains('amount')
    def validate_amount(self):
        if self.amount <= 0:
            raise exceptions.ValidationError('Amount paid must be greater than zero')


# Configuration
class Config(models.TransientModel):
    _inherit = "res.config.settings"
    _name = "tea.config.settings"
    default_unit_pay = fields.Float(default_model='tea.labor', string="Unit Pay", required=True)

    @api.constrains('default_unit_pay')
    def validate_unit_pay(self):
        if self.default_unit_pay <= 0:
            raise exceptions.ValidationError('Unit pay must be greater than zero.')
